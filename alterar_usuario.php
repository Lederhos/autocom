<?php
	session_start();

	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	@$id_usuario = filter_input(INPUT_GET, "usuario", FILTER_SANITIZE_SPECIAL_CHARS);

	if($id_usuario != $_SESSION["id"]){
		if(isset($_SESSION['adm'])){
			if($_SESSION['adm'] != true){
				header("Location: /arduinos.php");
				exit();
			}
		}else{
			header("Location: /arduinos.php");
			exit();
		}
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}else{
		$mensagem = "";
		$usuario = null;
		if(isset($_GET["usuario"])){
			$id_usuario = filter_input(INPUT_GET, "usuario", FILTER_SANITIZE_SPECIAL_CHARS);
			if(empty($id_usuario) || !is_numeric($id_usuario)){
				$mensagem = "Usuário não encontrado";
			}else{
				$resultado = mysqli_query($link, "SELECT * FROM usuarios WHERE id = $id_usuario AND id_conta = '$_SESSION[id_conta]'");
				if(@mysqli_num_rows($resultado) > 0){
					$usuario = mysqli_fetch_object($resultado);
				}else{
					$mensagem = "Usuário não encontrado.";
				}
			}
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Alterar Usuário</title>
	<?php include("inc/head.inc.php") ?>
</head>
<body>
	<div class="container-fluid">
		<?php include("inc/topo.inc.php"); ?>
		<div class="row">
			<div class="container">
				<div class="col s12">
					<h3 class="center-align"><?php echo $mensagem ?></h3>
					<h4 class="grey-text"><?php echo $usuario->nome ?></h4>
				</div>
				<div class="col s12">
					<?php
						if(isset($_POST["atualizar"])){
							$nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_SPECIAL_CHARS);
							$login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS);
							$adm = filter_input(INPUT_POST, "adm", FILTER_SANITIZE_SPECIAL_CHARS);
							$id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_SPECIAL_CHARS);
							
							if($adm == "true"){
								$adm = "1";
							}else{
								$adm = "0";
							}


							if(empty($nome) || empty($login) || empty($id)){
								echo "<p class='red-text'><b>Verifique os campos.</b></p>";
							}else{
								if ($_SESSION["adm"] == "1"){
									$atualizacao = mysqli_query($link, "UPDATE usuarios SET nome = '$nome', login = '$login', adm= '$adm' WHERE id = $id AND id_conta = $_SESSION[id_conta]");
									if($id_usuario == $_SESSION["id"]){
										$_SESSION["adm"] = $adm;
									}
								}else{
									$atualizacao = mysqli_query($link, "UPDATE usuarios SET nome = '$nome', login = '$login' WHERE id = $id AND id_conta = $_SESSION[id_conta]");
								}
								if($atualizacao){
									$_SESSION["mensagem"] = "Materialize.toast('Atualizado com Sucesso!', 5000, 'green')";
								}else
									$_SESSION["mensagem"] = "Materialize.toast('Ocorreu um erro ao realizar operação.', 7000, 'red')";
								mysqli_close($link);
								header("Location: /usuarios.php");
								exit();
							}
						}
					?>
					<form method="post">
						<input value="<?php echo $usuario->id ?>" name="id" id="id" type="hidden" class="validate">
						<div class="input-field col s6">
							<input value="<?php echo $usuario->nome ?>" name="nome" id="nome" type="text" class="validate" required>
							<label class="active" for="nome">Nome</label>
						</div>
						<div class="input-field col s6">
							<input value="<?php echo $usuario->login ?>" id="login" name="login" type="text" class="validate" required>
							<label class="active" for="login">Login</label>
						</div>
						<?php if ($_SESSION["adm"] == "1"): ?>
							<div class="input-field col s6">
								<select name="adm" id="adm" required>
									<option <?php echo $usuario->adm ? "selected" : "" ?> value="true">Administrador</option>
									<option <?php echo $usuario->adm ? "" : "selected" ?> value="false">Usuário</option>
								</select>
								<label for="adm">Adm</label>
							</div>
						<?php endif ?>
						<div class="col s12">
							<button class="btn blue right-align" type="submit" name="atualizar" value="atualizar">Atualizar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	  		Materialize.updateTextFields();
	  		$('select').material_select();
	  	});
	  </script>
	  <?php include("inc/script.inc.php") ?>
</body>
</html>