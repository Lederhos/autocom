<?php
	session_start();
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}

	$arduino = filter_input(INPUT_GET, "arduino", FILTER_SANITIZE_SPECIAL_CHARS);

	if(empty($arduino) || !is_numeric($arduino)){
		$_SESSION["mensagem"] = "Materialize.toast('Dispositivo não encontrado!', 7000, 'yellow')";
		header("Location: /arduinos.php");
		exit();
	}

	if ($_SESSION["adm"] === "1"){
		$resultado = mysqli_query($link, "SELECT * FROM arduinos WHERE arduinos.id = $arduino AND id_conta = $_SESSION[id_conta] LIMIT 1");
	}else{
		$resultado = mysqli_query($link, "SELECT * FROM arduinos JOIN usuarios_arduinos ON (usuarios_arduinos.id_arduino = arduinos.id) WHERE usuarios_arduinos.id_usuario = '$_SESSION[id]' AND arduinos.id = $arduino LIMIT 1");
	}

	$total = mysqli_num_rows($resultado);

	if($total == 0){
		$_SESSION["mensagem"] = "Materialize.toast('Dispositivo não encontrado!', 7000, 'yellow')";
		header("Location: /arduinos.php");
		exit();
	}

	$config = mysqli_fetch_object($resultado);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Arduino</title>
	<?php include("inc/head.inc.php") ?>
  	<style type="text/css">
  		.btn{
  			width: 100%;
  			margin-bottom: 1rem;
  		}

  		.card-panel span{
  			font-size: 5rem;
  		}

  		.acao:hover{
  			cursor: pointer;
  		}
  	</style>
</head>
<body>
	<div class="container-fluid">
		<?php include("inc/topo.inc.php"); ?>

		<div class="row">
			<div class="col s6 m12 acao" onclick="acao('ligamentoAutomatico')">
				<div class="card-panel grey ligamentoAutomatico">
				  <p class='white-text center-align'><i class="medium material-icons">wb_sunny</i></p>
				  <h4 class="center-align white-text">Auto</h4>
				</div>
			</div>
			<div class="col s6 m4 acao" onclick="acao('luz1')">
				<div class="card-panel grey luz1">
				  <p class='white-text center-align'><i class="medium material-icons">wb_incandescent</i></p>
				  <h4 class="center-align white-text">1</h4>
				</div>
			</div>
			<div class="col s6 m4 acao" onclick="acao('luz2')">
				<div class="card-panel grey luz2">
				  <p class='white-text center-align'><i class="medium material-icons">wb_incandescent</i></p>
				  <h4 class="center-align white-text">2</h4>
				</div>
			</div>

			<div class="col s6 m4 acao" onclick="acao('luz3')">
				<div class="card-panel grey luz3">
				  <p class='white-text center-align'><i class="medium material-icons">wb_incandescent</i></p>
				  <h4 class="center-align white-text">3</h4>
				</div>
			</div>

			<div class="col s6 acao" onclick="acao('ventilador')">
				<div class="card-panel grey ventilador">
				  <p class='white-text center-align'><i class="medium material-icons">toys</i></p>
				</div>
			</div>

			<div class="col s6 acao" onclick="acao('portao')">
				<div class="card-panel grey portao">
				  <p class='white-text center-align portao_aberto'>
				  	<i class="medium material-icons">lock_open</i>
				  	<!-- <i class="medium material-icons">last_page</i> -->
				  </p>
				  <p class='white-text center-align portao_fechado' style="display: none;">
				  	<i class="medium material-icons">lock_outline</i>
				  	<!-- <i class="medium material-icons">first_page</i> -->
				  </p>
				  <!-- <h4 class='white-text center-align'>Portão Fechado</h4> -->
				</div>
			</div>

			<!-- <div class="col s12">
				<button class="btn luz1" value="luz1">Luz 1</button>
			</div>
			<div class="col s12">
				<button class="btn luz2" value="luz2">Luz 2</button>
			</div>
			<div class="col s12">
				<button class="btn luz3" value="luz3">Luz 3</button>
			</div>
			<div class="col s12">
				<button class="btn ventilador" value="ventilador">Ventilador</button>
			</div>
			<div class="col s12">
			<button class="btn abreportao" value="abreportao">Abrir Portão</button>
			</div>
			<div class="col s12">
				<button class="btn fechaportao" value="fechaportao">Fechar Portão</button>
			</div> -->
		</div>
	</div>
	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<script type="text/javascript">
		var endereco_local = "<?php echo $config->endereco_local ?>";
		var endereco_internet = "<?php echo $config->endereco_internet ?>";
		var endereco = endereco_local;
		var intervalo = 5000;
		var window_focus = true;
		

		$(window).focus(function() {
		    window_focus = true;
		}).blur(function() {
		    window_focus = false;
		});

		$(document).ready(function(){
			atualizarValores();
			// $(".btn").click(function(){
			// 	acao = $(this).val();
			// 	$.post(endereco+"?"+acao, {}, function(result){
					
			// 	});
			// });

			setInterval(atualizarValores, intervalo);

			$(document).keypress(function(data){
				comando = "";
				if(data.key == "1"){
					comando = "luz1";
				}
				if(data.key == "2"){
					comando = "luz2";
				}
				if(data.key == "3"){
					comando = "luz3";
				}
				if(data.key == "4" || data.key == "v"){
					comando = "ventilador";
				}
				if(data.key == "5" || data.key == "p"){
					comando = "portao";
				}
				if(data.key == "0" || data.key == "a"){
					comando = "ligamentoAutomatico";
				}
				acao(comando);
			});
		});

		function acao(comando){
			$.post(endereco+"/?"+comando, {}, function(result){
				atualizarValores();
			});
		}

		function atualizarValores(){
			if(window_focus)
				$.ajax({url: endereco+"?json", 
					timeout:intervalo,
					error: function(xhr, status, error){
						$(".acao .card-panel").removeClass("grey");
						$(".acao .card-panel").removeClass("blue-grey");
						$(".acao .card-panel").removeClass("blue");
						$(".acao .card-panel").addClass("grey");
						Materialize.toast("Conectando com a placa...", 3000, "blue");
						if(endereco == endereco_local){
							endereco = endereco_internet;
						}else{
							endereco = endereco_local;
						}
					},
					success: function(result){
						$(".acao .card-panel").removeClass("grey");
			        	if(result.ligamentoAutomatico == "true"){
			        		$(".ligamentoAutomatico").removeClass("grey");
			        		$(".ligamentoAutomatico").removeClass("blue-grey");
			        		$(".ligamentoAutomatico").addClass("blue");
			        	}else{
			        		$(".ligamentoAutomatico").removeClass("blue");
			        		$(".ligamentoAutomatico").addClass("blue-grey");
			        	}

			        	if(result.luz1 == "true"){
			        		$(".luz1").removeClass("grey");
			        		$(".luz1").removeClass("blue-grey");
			        		$(".luz1").addClass("blue");
			        	}else{
			        		$(".luz1").removeClass("blue");
			        		$(".luz1").addClass("blue-grey");
			        	}

			        	if(result.luz2 == "true"){
			        		$(".luz2").removeClass("grey");
			        		$(".luz2").removeClass("blue-grey");
			        		$(".luz2").addClass("blue");
			        	}else{
			        		$(".luz2").removeClass("blue");
			        		$(".luz2").addClass("blue-grey");
			        	}

			        	if(result.luz3 == "true"){
			        		$(".luz3").removeClass("grey");
			        		$(".luz3").removeClass("blue-grey");
			        		$(".luz3").addClass("blue");
			        	}else{
			        		$(".luz3").removeClass("blue");
			        		$(".luz3").addClass("blue-grey");
			        	}

			        	if(result.statusVentilador == "true"){
			        		$(".ventilador").removeClass("grey");
			        		$(".ventilador").removeClass("blue-grey");
			        		$(".ventilador").addClass("blue");
			        	}else{
			        		$(".ventilador").removeClass("blue");
			        		$(".ventilador").addClass("blue-grey");
			        	}

			        	if(result.portaoFechado == "false"){
			        		$(".portao").removeClass("grey");
			        		$(".portao").removeClass("blue-grey");
			        		$(".portao").addClass("blue");
			        		$(".portao_fechado").hide();
			        		$(".portao_aberto").show();

			        		$(".abreportao").removeClass("blue");
			        		$(".fechaportao").addClass("blue");
			        		$(".abreportao").prop("disabled", true);
			        		$(".fechaportao").prop("disabled", false);
			        	}else{
			        		$(".portao").removeClass("grey");
			        		$(".portao").removeClass("blue");
			        		$(".portao").addClass("blue-grey");
			        		$(".portao_fechado").show();
			        		$(".portao_aberto").hide();

			        		$(".fechaportao").removeClass("blue");
			        		$(".abreportao").addClass("blue");
			        		$(".fechaportao").prop("disabled", true);
			        		$(".abreportao").prop("disabled", false);
			        	}
				    }
				});
		}
	</script>
	<?php include("inc/script.inc.php") ?>
</body>
</html>