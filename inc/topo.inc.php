<header class="navbar-fixed" style="margin-bottom: 15px">
	<nav class="blue darken-1">
	  <div class="nav-wrapper">
	  	<a href="/arduino.php" class="brand-logo left"><span class='white-text'>AutoCom</span></a>
	    <ul class="right">
			<li><a href="/arduinos.php"><b>Dispositivos</b></a></li>
	      <?php
	      	if(isset($_SESSION['adm'])){
	      		if($_SESSION['adm'] == true){
	      			?>
	      			<!-- <li><a href="/configuracoes.php">Configurações</a></li> -->
	      			<li><a href="/usuarios.php"><b>Usuários</b></a></li>
	      			<?php
	      		}
	      	}
	      ?>
	      <li><a href="/alterar_usuario.php?usuario=<?php echo $_SESSION['id'] ?>"><b>Perfil</b></a></li>
	      <li><a href="/sair.php"><b>Sair</b></a></li>
	    </ul>
	  </div>
	</nav>
</header>