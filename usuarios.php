<?php
	session_start();
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	if(isset($_SESSION['adm'])){
		if($_SESSION['adm'] != true){
			header("Location: /arduinos.php");
			exit();
		}
	}else{
		$_SESSION["mensagem"] = "Materialize.toast('Você não possui permissão.', 7000, 'red')";
		header("Location: /arduinos.php");
		exit();
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Configurações</title>
	<?php include("inc/head.inc.php") ?>
</head>
<body>
	<div>
		<?php include("inc/topo.inc.php"); ?>
		<?php

			$total_adm = mysqli_query($link, "SELECT COUNT(id) as total FROM usuarios WHERE adm = 1 AND id_conta = '$_SESSION[id_conta]'");
			$total_adm = mysqli_fetch_object($total_adm);
			$total_adm = $total_adm->total;

			$total_usuarios = mysqli_query($link, "SELECT COUNT(id) as total FROM usuarios WHERE id_conta = '$_SESSION[id_conta]'");
			$total_usuarios = mysqli_fetch_object($total_usuarios);
			$total_usuarios = $total_usuarios->total;

			$page_rows = 5;
			$last = ceil($total_usuarios/$page_rows);
			if($last < 1){
				$last = 1;
			}

			$pagenum = 1;
			if(isset($_GET['pn'])){
				$pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
			}

			if ($pagenum < 1) { 
				$pagenum = 1; 
			} 
			else if ($pagenum > $last) { 
				$pagenum = $last; 
			}

			$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;


			$usuarios = mysqli_query($link, "SELECT * FROM usuarios WHERE id_conta = '$_SESSION[id_conta]' $limit");
		?>
		<div class="row">
			<div class="col s12">
				<div class="clearfix"></div>
				<div class="left"><h4 class="grey-text">Usuários</h4></div>
				<div class="right">
					<a href="/cadastrar_usuario.php" class="btn green">Cadastrar</a>
				</div>
			</div>
			<div class="col s12">
				<table class="striped">
					<tr>
						<th>ID</th>
						<th>Nome</th>
						<th>Login</th>
						<th>Alterar</th>
					</tr>
					<?php while($usuario = mysqli_fetch_object($usuarios)){ ?>
					<tr>
						<td><?php echo $usuario->id ?></td>
						<td><?php echo $usuario->nome ?></td>
						<td><?php echo $usuario->login ?></td>
						<td>
							<a href='/alterar_usuario.php?usuario=<?php echo $usuario->id ?>' class="btn waves-effect waves-light blue">Alterar</a>
							<?php if ($total_adm > 1 || $usuario->adm != 1): ?>
								<button class="btn red" onclick="excluir(<?php echo $usuario->id ?>)">Excluir</button>
							<?php endif ?>
						</td>
					</tr>
					<?php } ?>
				</table>
				<ul class="pagination">
				    <li class="disabled"><a href="/usuarios.php"><i class="material-icons">chevron_left</i></a></li>
				    <?php
				    	for ($i=1; $i <= $last ; $i++) { 
				    		if($pagenum == $i){
				    			$active = "active";
				    		}else{
				    			$active = "";
				    		}
			    			echo "<li class='$active waves-effect'><a href='/usuarios.php?pn=$i''>$i</a></li>";
				    	}
				    ?>
				    <li class="waves-effect"><a href="/usuarios.php?pn=<?php echo $last ?>"><i class="material-icons">chevron_right</i></a></li>
				  </ul>
			</div>
		</div>
	</div>
	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	  		Materialize.updateTextFields();
	  	});

	  	function excluir(usuario){
	  		if(confirm("Tem certeza?")){
  				$.ajax({url: "/excluir_usuario.php?usuario="+usuario, 
  					error: function(xhr, status, error){
  						alert("Ocorreu um erro.");
  					},
  					success: function(result){
  						location.reload();
  				    }
				});
	  		}
	  	}
	  </script>
	  <?php include("inc/script.inc.php") ?>
</body>
</html>