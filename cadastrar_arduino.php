<?php
	session_start();

	$mensagem = "";
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	if(isset($_SESSION['adm'])){
		if($_SESSION['adm'] != true){
			header("Location: /arduinos.php");
			exit();
		}
	}else{
		header("Location: /arduinos.php");
		exit();
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    die("Erro. Não foi possível conectar no banco de dados!");
	    exit;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Arduino</title>
	<?php include("inc/head.inc.php") ?>
</head>
<body>
	<div class="container-fluid">
		<?php include("inc/topo.inc.php"); ?>
		<div class="row">
			<div class="container">
				<div class="col s12">
					<h3 class="center-align"><?php echo $mensagem ?></h3>
					<h4 class="grey-text">Cadastrar Dispositivo</h4>
				</div>
				<div class="col s12">
					<?php
						if(isset($_POST["cadastrar"])){
							$nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_SPECIAL_CHARS);
							$endereco_local = filter_input(INPUT_POST, "endereco_local", FILTER_SANITIZE_SPECIAL_CHARS);
							$endereco_internet = filter_input(INPUT_POST, "endereco_internet", FILTER_SANITIZE_SPECIAL_CHARS);

							if(empty($nome)){
								echo "<p class='red-text'><b>Verifique os campos.</b></p>";
							}else{
								$cadastro = mysqli_query($link, "INSERT INTO arduinos (nome, endereco_local, endereco_internet, id_conta) VALUES ('$nome', '$endereco_local', '$endereco_internet', '$_SESSION[id_conta]')");
								if($cadastro){
									$_SESSION["mensagem"] = "Materialize.toast('Cadastrado com Sucesso!', 5000, 'green');";
									$ultimo = mysqli_query($link, "SELECT MAX(id) as id FROM arduinos WHERE id_conta = $_SESSION[id_conta]");
									$ultimo = mysqli_fetch_object($ultimo);
									$cadastro = mysqli_query($link, "INSERT INTO usuarios_arduinos (id_usuario, id_arduino) VALUES ('$_SESSION[id]', $ultimo->id)");
									$_SESSION["mensagem"] .= "Materialize.toast('Vinculado à sua conta!', 7000, 'blue');";
								}
								mysqli_close($link);
								header("Location: /configuracoes.php?arduino=$ultimo->id");
								exit();
							}
						}
					?>
					<form method="post">
						<div class="row">
							<div class="input-field col s12 m6 offset-m3">
								<input name="nome" id="nome" type="text" class="validate" required>
								<label class="active" for="nome">Nome</label>
							</div>
							<div class="input-field col s12 m6 offset-m3">
								<input name="endereco_local" id="endereco_local" type="text" class="validate" placeholder="http://0.0.0.0">
								<label class="active" for="endereco_local">Endereço local</label>
							</div>
							<div class="input-field col s12 m6 offset-m3">
								<input name="endereco_internet" id="endereco_internet" type="text" class="validate" placeholder="http://dominio.com">
								<label class="active" for="endereco_internet">Endereço na Internet</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m6 offset-m3">
								<button class="btn blue" type="submit" name="cadastrar" value="cadastrar">Cadastrar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	  		Materialize.updateTextFields();
	  		$('select').material_select();
	  	});
	  </script>
	  <?php include("inc/script.inc.php") ?>
</body>
</html>