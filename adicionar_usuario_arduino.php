<?php
	session_start();
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			exit();
		}
	}else{
		exit();
	}

	if(isset($_SESSION['adm'])){
		if($_SESSION['adm'] != true){
			exit();
		}
	}else{
		exit();
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}

	$arduino = filter_input(INPUT_GET, "arduino", FILTER_SANITIZE_SPECIAL_CHARS);
	$usuario = filter_input(INPUT_GET, "usuario", FILTER_SANITIZE_SPECIAL_CHARS);


	$consulta = mysqli_query($link, "SELECT id FROM usuarios WHERE id_conta = $_SESSION[id_conta] AND usuarios.id = $usuario");//testando se usuário existe e se pertence a mesma conta
	$consulta = mysqli_num_rows($consulta);

	if($consulta > 0){
		$add = mysqli_query($link, "INSERT INTO usuarios_arduinos (id_usuario, id_arduino) VALUES ($usuario, $arduino)");
		if($add){
			$_SESSION["mensagem"] = "Materialize.toast('Vinculado com Sucesso!', 5000, 'blue')";
		}
	}else{
		$_SESSION["mensagem"] = "Materialize.toast('Usuário não encontrado!', 5000, 'yellow')";
	}