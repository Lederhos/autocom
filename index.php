<?php 
	session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
		html, body, main, .container-fluid, .row, .valign-wrapper{
			height: 100%;
		}
		.input-field input:focus + label {
		   color: #2196F3 !important;
		 }
		 .row .input-field input:focus {
		   border-bottom: 1px solid #2196F3 !important;
		   box-shadow: 0 1px 0 0 #2196F3 !important
		 }
		 .btn-block{
		 	width: 100%;
		 }
	</style>
</head>
<body class="blue-grey lighten-5">
	<main>
		<div class="container-fluid">
			<div class="row">
				<div class='valign-wrapper'>
					<div class="col s12 m10 offset-m1 l6 offset-l3">
						<div class="card">
							<?php
								$mensagem = "";
								if(isset($_POST['entrar'])){
									if(!empty($_POST['entrar'])){
										if(empty($_POST['login'] || empty($_POST["senha"]))){
											$mensagem = "Informe todos os campos";
										}else{
											include "inc/banco.inc.php";
											if (!$link) {
	    									echo "Erro. Não foi possível conectar no banco de dados!";
	   										exit;
											}

											$login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS);
											$senha = filter_input(INPUT_POST, "senha", FILTER_SANITIZE_SPECIAL_CHARS);

											$senha = sha1($senha);

											$resultado = mysqli_query($link, "SELECT * FROM usuarios WHERE login LIKE '$login' AND senha = '$senha'");
											if(!$resultado){
												echo mysqli_errno($link);
											}
											$total_usuarios = mysqli_num_rows($resultado);
											if($total_usuarios == 0){
												$mensagem = "Usuário e senha não combinam.";
											}else{
												$usuario = mysqli_fetch_object($resultado);
												$_SESSION["logado"] = true;
												$_SESSION["id"] = $usuario->id;
												$_SESSION["adm"] = $usuario->adm;
												$_SESSION["id_conta"] = $usuario->id_conta;

												date_default_timezone_set("America/Sao_Paulo");
												$data = date("Y-m-d H:i:s");

												$gravar_historico = mysqli_query($link, "INSERT INTO historico (id_usuario, hora) VALUES ($usuario->id, '$data')");
												header("Location: /arduinos.php");

												exit();
											}


											mysqli_close($link);
										}
									}
								}
							?>
							<form method="post" action="/">
								<div class="card-content">
									<span class="card-title center blue-text"><b>Arduino</b></span>
									<div class="input-field">
										<label for="login">Login</label>
										<input autofocus type="text" name="login" id="login" class='validate center-align' >
									</div>
									<div class="input-field">
										<label for="senha">Senha</label>
										<input autofocus type="password" name="senha" id="senha" class='validate center-align' >
										<span class='red-text'><?php echo $mensagem ?></span>
									</div>
								</div>
								<div class="card-action">
									<button type="submit" class='btn waves-effect waves-light btn-block btn-large blue white-text' name='entrar' value="entrar"><b>Entrar</b></button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<footer>
		
	</footer>
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			Materialize.updateTextFields();
		});
	</script>
	<?php include("inc/script.inc.php") ?>
</body>
</html>