<?php
	session_start();
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	if(isset($_SESSION['adm'])){
		if($_SESSION['adm'] != true){
			header("Location: /arduinos.php");
			exit();
		}
	}else{
		$_SESSION["mensagem"] = "Materialize.toast('Você não possui permissão!', 7000, 'red');";
		header("Location: /arduinos.php");
		exit();
	}

	include "inc/banco.inc.php";//$link = mysqli_connect("localhost", "arduinocom", "arduinocom", "arduinocom");
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}

	$arduino = filter_input(INPUT_GET, "arduino", FILTER_SANITIZE_SPECIAL_CHARS);
	if(empty($arduino) || !is_numeric($arduino)){
		$_SESSION["mensagem"] = "Materialize.toast('Dispositivo não encontrado!', 7000, 'yellow');";
		header("Location: /arduinos.php");
		exit();
	}

	if($_SESSION["adm"] === "1"){
		$resultado = mysqli_query($link, "SELECT * FROM arduinos WHERE arduinos.id = $arduino AND id_conta = $_SESSION[id_conta] LIMIT 1");
	}else{
		$resultado = mysqli_query($link, "SELECT * FROM arduinos JOIN usuarios_arduinos ON (usuarios_arduinos.id_arduino = arduinos.id) WHERE usuarios_arduinos.id_usuario = '$_SESSION[id]' AND arduinos.id = $arduino LIMIT 1");
	}

	$total = mysqli_num_rows($resultado);

	if($total == 0){
		$_SESSION["mensagem"] = "Materialize.toast('Dispositivo não encontrado!', 7000, 'yellow');";
		header("Location: /arduinos.php");
		exit();
	}

	if(isset($_POST["configurar"])){
		$nome = utf8_decode(filter_input(INPUT_POST, "nome", FILTER_SANITIZE_SPECIAL_CHARS));
		$endereco_local = utf8_decode(filter_input(INPUT_POST, "endereco_local", FILTER_SANITIZE_SPECIAL_CHARS));
		$endereco_internet = utf8_decode(filter_input(INPUT_POST, "endereco_internet", FILTER_SANITIZE_SPECIAL_CHARS));

		$atualizacao = mysqli_query($link, "UPDATE arduinos SET nome='$nome', endereco_local='$endereco_local', endereco_internet='$endereco_internet' WHERE id = $arduino");
		if($atualizacao){
			$_SESSION["mensagem"] = "Materialize.toast('Atualizado com Sucesso!', 5000, 'green');";
			header("Location: /arduinos.php");
			exit();
		}
	}

	$config = mysqli_fetch_object($resultado);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Configurações</title>
	<?php include("inc/head.inc.php") ?>
</head>
<body>
	<div>
		<?php include("inc/topo.inc.php"); ?>

		<div class="row">
			<div class="col s12">
				<h4 class="grey-text">Configurações</h4>
				<hr />
				<form method="post">
					<div class="row">
						<div class="input-field col s12 m6 offset-m3">
							<input value="<?php echo utf8_encode($config->nome) ?>" name="nome" id="nome" type="text" class="validate">
							<label class="active" for="nome">Nome</label>
						</div>
						<div class="input-field col s12 m6 offset-m3">
							<input value="<?php echo utf8_encode($config->endereco_local) ?>" name="endereco_local" id="endereco_local" type="text" class="validate ip">
							<label class="active" for="endereco_local">Endereço local</label>
						</div>
						<div class="input-field col s12 m6 offset-m3">
							<input value="<?php echo utf8_encode($config->endereco_internet) ?>" name="endereco_internet" id="endereco_internet" type="text" class="validate ip">
							<label class="active" for="endereco_internet">Endereço na Internet</label>
						</div>
					</div>
					<div class="row">
						<div class="col s12 m6 offset-m3">
							<button class="btn blue waves-effect waves-light" type="submit" name="configurar" value="configurar">Alterar Configurações</button>
							<button class="btn red excluir waves-effect waves-light" type="button" name="excluir" value="<?php echo $config->id ?>">Excluir</button>
							<a class="waves-effect waves-light btn modal-trigger indigo" href="#modal1">Usuários</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Modal Structure -->
	  <div id="modal1" class="modal modal-fixed-footer">
	    <div class="modal-content">
	      <h4>Usuários com Acesso</h4>
	      <div class="row">
		      <div class="input-field col s8">
		      	<select id="adicionar_usuario">
		      		<option value="" disabled selected>Selecione</option>
		      		<?php
		      			$usuarios = mysqli_query($link, "SELECT id, nome FROM usuarios WHERE id_conta = $_SESSION[id_conta] AND usuarios.id NOT IN (SELECT id_usuario FROM usuarios_arduinos WHERE id_arduino = $config->id)");

		      			while($usuario = mysqli_fetch_object($usuarios)){
		      				echo "<option value='$usuario->id'>$usuario->nome</option>";
		      			}
		      		?>
		      	</select>
		      	<label>Usuário</label>
		      </div>
		      <div class="col s4">
		      	<button class="btn waves-effect waves-light blue add">Add</button>
		      </div>
	      </div>
	      <hr>
	      <div class="row">
		      <table class="responsive-table">
		      	<tr>
		      		<th>Usuário</th>
		      		<th></th>
		      	</tr>
			      <?php
			      	$usuarios_arduino = mysqli_query($link, "SELECT usuarios.nome, usuarios.id FROM usuarios_arduinos JOIN usuarios ON (usuarios.id = usuarios_arduinos.id_usuario) WHERE usuarios_arduinos.id_arduino = $config->id AND usuarios.id_conta = $_SESSION[id_conta]");
			      	while($usuario = mysqli_fetch_object($usuarios_arduino)){
			      		echo "<tr><td>$usuario->nome</td><td><button class='btn red waves-effect waves-light remover' value='$usuario->id'>Remover Acesso</button></td></tr>";
			      	}
			      ?>
		      </table>
    	  </div>
	    </div>
	    <div class="modal-footer">
	      <a class="modal-action modal-close waves-effect waves-green btn-flat ">Fechar</a>
	    </div>
	  </div>
	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	  <script type="text/javascript" src="/js/jquery.mask.js"></script>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	  		$(".ip").mask('http://0ZZ.0ZZ.0ZZ.0ZZ', {translation: {'Z': {pattern: /[0-9]/, optional: true}}});

	  		Materialize.updateTextFields();
	  		$('.modal').modal();
	  		$('select').material_select();

		  	$(".excluir").click(function(){
		  		if(confirm("Tem certeza?")){
	  				$.ajax({url: "/excluir_dispositivo.php?arduino="+$(this).val(), 
	  					error: function(xhr, status, error){
	  						alert("Ocorreu um erro.");
	  					},
	  					success: function(result){
	  						location.href = "/arduinos.php";
	  				    }
					});
		  		}
		  	});

		  	$(".remover").click(function(){
		  		if(confirm("Tem certeza?")){
	  				$.ajax({url: "/remover_usuario_arduino.php?usuario="+$(this).val()+"&arduino=<?php echo $config->id ?>",
	  					error: function(xhr, status, error){
	  						alert("Ocorreu um erro.");
	  					},
	  					success: function(result){
	  						// location.href = "/arduinos.php";
	  						location.reload();
	  				    }
					});
		  		}
		  	});

		  	$(".add").click(function(){
		  		if(confirm("Tem certeza?")){
	  				$.ajax({url: "/adicionar_usuario_arduino.php?usuario="+$("#adicionar_usuario").val()+"&arduino=<?php echo $config->id ?>",
	  					error: function(xhr, status, error){
	  						alert("Ocorreu um erro.");
	  					},
	  					success: function(result){
	  						// location.href = "/arduinos.php";
	  						location.reload();
	  				    }
					});
		  		}
		  	});
	  	});
	  </script>
	  <?php include("inc/script.inc.php") ?>
</body>
</html>