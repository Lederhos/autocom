<?php
	session_start();
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Arduino</title>
	<?php include("inc/head.inc.php") ?>
  	<style type="text/css">
  		.btn{
  			width: 100%;
  			margin-bottom: 1rem;
  		}

  		.card-panel span{
  			font-size: 5rem;
  		}

  		.acao:hover{
  			cursor: pointer;
  		}
  	</style>
</head>
<body>
	<div class="container-fluid">
		<?php include("inc/topo.inc.php"); ?>
		<div class="row" style="padding-top: 30px;">
			<h2 class="center-align blue-grey-text">Selecione um Dispositivo</h2>
		</div>
		<div class="container">
			<div class="row">
				<div class="center-align">
					<table>
						<tr>
							<th></th>
							<?php if ($_SESSION["adm"] === "1"): ?>
								<th></th>
							<?php endif ?>
						</tr>
						<?php
							if ($_SESSION["adm"] === "1"){
								$resultado = mysqli_query($link, "SELECT arduinos.id, arduinos.nome FROM arduinos WHERE arduinos.id_conta = $_SESSION[id_conta]");
							}else{
								$resultado = mysqli_query($link, "SELECT arduinos.id, arduinos.nome FROM arduinos JOIN usuarios_arduinos ON (usuarios_arduinos.id_arduino = arduinos.id) WHERE usuarios_arduinos.id_usuario = '$_SESSION[id]'");
							}
							while($config = mysqli_fetch_object($resultado)){
								?>
									<tr>
										<td>
											<a href="arduino.php?arduino=<?php echo $config->id ?>" class="waves-effect waves-light btn-large indigo btn-block" style="margin-bottom: 10px;"><i class="material-icons left">cloud</i><?php echo utf8_encode($config->nome) ?></a>
										</td>
										<?php if ($_SESSION["adm"] === "1"): ?>
											<td>
												<a href="configuracoes.php?arduino=<?php echo $config->id ?>" class="waves-effect waves-light btn-large btn-block red" style="margin-bottom: 10px;"><i class="material-icons left">mode_edit</i>Alterar</a>
											</td>
										<?php endif ?>
									</tr>
								<?php
							}
						?>
					</table>
				</div>
				<?php if ($_SESSION["adm"] === "1"): ?>
					<div class="right-align">
						<a class="btn-floating btn-large waves-effect waves-light red" href="/cadastrar_arduino.php"><i class="material-icons">add</i></a>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	<?php include("inc/script.inc.php") ?>
</body>
</html>