<meta charset="utf-8" />
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style type="text/css">
	.btn-block{
		width: 100% !important;
	}

	.brand-logo{
		transition: all 0.2s;
	}

	@media screen and (max-width: 378px){
		.brand-logo{
			font-size: 1.5rem !important;
		}
	}

	@media screen and (max-width: 338px){
		.brand-logo{
			font-size: 0rem !important;
		}
	}
</style>