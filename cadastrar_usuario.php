<?php
	session_start();

	$mensagem = "";
	if(isset($_SESSION['logado'])){
		if($_SESSION['logado'] != true){
			header("Location: /");
			exit();
		}
	}else{
		header("Location: /");
		exit();
	}

	if(isset($_SESSION['adm'])){
		if($_SESSION['adm'] != true){
			header("Location: /arduinos.php");
			exit();
		}
	}else{
		header("Location: /arduinos.php");
		exit();
	}

	include "inc/banco.inc.php";
	if (!$link) {
	    echo "Erro. Não foi possível conectar no banco de dados!";
	    exit;
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Arduino</title>
	<?php include("inc/head.inc.php") ?>
</head>
<body>
	<div class="container-fluid">
		<?php include("inc/topo.inc.php"); ?>
		<div class="row">
			<div class="container">
				<div class="col s12">
					<h3 class="center-align"><?php echo $mensagem ?></h3>
					<h4 class="grey-text">Cadastrar Usuário</h4>
				</div>
				<div class="col s12">
					<?php
						if(isset($_POST["atualizar"])){
							$nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_SPECIAL_CHARS);
							$login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_SPECIAL_CHARS);
							$senha = filter_input(INPUT_POST, "senha", FILTER_SANITIZE_SPECIAL_CHARS);
							$senha = sha1($senha);
							$adm = filter_input(INPUT_POST, "adm", FILTER_SANITIZE_SPECIAL_CHARS);

							if($adm == "true"){
								$adm = "1";
							}else{
								$adm = "0";
							}

							if(empty($nome) || empty($login)|| empty($senha)){
								echo "<p class='red-text'><b>Verifique os campos.</b></p>";
							}else{
								echo "ADM = ".$_SESSION["adm"];
								if ($_SESSION["adm"] == "1"){
									$cadastro = mysqli_query($link, "INSERT INTO usuarios (nome, login, senha, adm, id_conta) VALUES ('$nome', '$login', '$senha', '$adm', $_SESSION[id_conta])");
									echo "INSERT INTO usuarios (nome, login, senha, adm, id_conta) VALUES ('$nome', '$login', '$senha', '$adm', $_SESSION[id_conta]";
								
								if($cadastro){
									$_SESSION["mensagem"] = "Materialize.toast('Cadastrado com Sucesso!', 5000, 'green')";
								}
								mysqli_close($link);
								header("Location: /usuarios.php");
								exit();
							}
						}
					}
					?>
					<form method="post">
						<input name="id" id="id" type="hidden" class="validate">
						<div class="input-field col s6">
							<input name="nome" id="nome" type="text" class="validate">
							<label class="active" for="nome">Nome</label>
						</div>
						<div class="input-field col s6">
							<input id="login" name="login" type="text" class="validate">
							<label class="active" for="login">Login</label>
						</div>
						<div class="input-field col s6">
							<input id="senha" name="senha" type="password" class="validate">
							<label class="active" for="senha">Senha</label>
						</div>
						<div class="input-field col s6">
							<select name="adm" id="adm">
								<option value="true">Administrador</option>
								<option value="false">Usuário</option>
							</select>
							<label for="adm">Adm</label>
						</div>
						<div class="col s12">
							<button class="btn blue right-align" type="submit" name="atualizar" value="atualizar">Cadastrar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<!-- JQUERY -->
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	  crossorigin="anonymous"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	  <script type="text/javascript">
	  	$(document).ready(function(){
	  		Materialize.updateTextFields();
	  		$('select').material_select();
	  	});
	 
	  </script>
	  <?php include("inc/script.inc.php") ?>
</body>
</html>